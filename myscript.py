import os
import logging

import mysql.connector


def connect_to_database():
    host = os.getenv("DATABASE_HOST")
    user = os.getenv("DATABASE_USER")
    password = os.getenv("DATABASE_PASSWORD")
    database = os.getenv("DATABASE_NAME")
    logging.debug("[DB] Connecting to host %s", host)

    while True:
        try:
            connection = mysql.connector.connect(
                host=host,
                user=user,
                passwd=password,
                database=database,
                charset='utf8mb4',
                use_pure=True,
                connection_timeout=5)
            return connection
        except mysql.connector.Error:
            logging.error("Cannot connect to the database")
            return None


def get_number_of_users():
  database = connect_to_database()
  if database is None:
    return None
  cursor = database.cursor()
  query = "SELECT COUNT(*) FROM users"
  cursor.execute(query)
  res = cursor.fetchall()
  cursor.close()
  return res[0][0]



number_users = get_number_of_users()
print(f"Number of users {number_users}")


